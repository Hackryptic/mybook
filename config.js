const config = {
  gatsby: {
    pathPrefix: '/',
    siteUrl: 'https://hkbook.gatsbyjs.io',
    gaTrackingId: null,
    trailingSlash: false,
  },
  header: {
    logo: 'https://drive.google.com/uc?export=download&id=1te25oskMRmeELFLjEho_0v5tClmZYFdc',
    logoLink: 'https://hkbook.gatsbyjs.io',
    title: "",
    githubUrl: 'https://github.com/WICWIU/WICWIU',
    helpUrl: '',
    tweetText: '',
    social: '',    
    links: [{ text: '', link: '' }],
    search: {
      enabled: true,
      indexName: 'bookindex',
      algoliaAppId: process.env.GATSBY_ALGOLIA_APP_ID,
      algoliaSearchKey: process.env.GATSBY_ALGOLIA_SEARCH_KEY,
      algoliaAdminKey: process.env.ALGOLIA_ADMIN_KEY,
    },
  },
  sidebar: {
    forcedNavOrder: [
      '/test', // add trailing slash if enabled above
      '/subtest',
    ],
    collapsedNav: [
      '/subtest', // add trailing slash if enabled above
    ],
    links: [{ text: 'My Blog', link: 'https://hackrypticblog.gatsbyjs.io' }],
    frontline: false,
    ignoreIndex: true,
    title:
      "<a href='https://hasura.io/learn/'>graphql </a><div class='greenCircle'></div><a href='https://hasura.io/learn/graphql/react/introduction/'>react</a>",
  },
  siteMetadata: {
    title: 'Gatsby Gitbook Boilerplate | Hasura',
    description: 'Documentation built with mdx. Powering hasura.io/learn ',
    ogImage: null,
    docsLocation: 'https://github.com/hasura/gatsby-gitbook-boilerplate/tree/master/content',
    favicon: 'https://graphql-engine-cdn.hasura.io/img/hasura_icon_black.svg',
  },
  pwa: {
    enabled: false, // disabling this will also remove the existing service worker.
    manifest: {
      name: 'Gatsby Gitbook Starter',
      short_name: 'GitbookStarter',
      start_url: '/',
      background_color: '#6b37bf',
      theme_color: '#6b37bf',
      display: 'standalone',
      crossOrigin: 'use-credentials',
      icons: [
        {
          src: 'src/pwa-512.png',
          sizes: `512x512`,
          type: `image/png`,
        },
      ],
    },
  },
};

module.exports = config;
